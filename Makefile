build:
	clear
	rebar clean skip_deps=true
	rebar compile skip_deps=true

run:
	clear
	ERL_LIBS=apps:deps \
	ERL_MAX_PORTS=20000 \
	erl \
	-name chat_server \
	-s chat_server \
	-boot start_sasl \
 	-sasl errlog_type error \
  -config config/app.config

#------------------------------------------------
# basho bench
#------------------------------------------------
BASHO_BENCH_DIR=../../_erl/basho_bench
BENCH_RESULTS_LOCAL_DIR=./results

CHAT_CONFIG_FILE=chat_driver.config
CHAT_CONFIG=./apps/bench/priv/${CHAT_CONFIG_FILE}

# -----------------------------------------------
# build {code_paths, [...]).}
# -----------------------------------------------
space :=
space +=
comma := ,
join-with = $(subst $(space),$1,$(strip $2))
find_dirs = $(wildcard $(dir)/*)
apps := $(foreach dir, ${PWD}/apps, $(find_dirs))
deps := $(foreach dir, ${PWD}/deps, $(find_dirs))
paths := $(foreach dir,${apps}${deps},"\"$(dir)/ebin\"")
code_paths := "{code_paths, [$(call join-with, ${comma}, $(paths))]}."

test_dir := ${PWD}/bench_results

bench:
	@clear
	@mkdir -p ${test_dir}/tmp

	@cp ${CHAT_CONFIG} ${BASHO_BENCH_DIR}/${CHAT_CONFIG_FILE}
	@echo "${code_paths}" >> ${BASHO_BENCH_DIR}/${CHAT_CONFIG_FILE}
# 	@cd ${BASHO_BENCH_DIR}; ./basho_bench --bench-name="parse_params_all_in_series_and_response" --results-dir="${test_dir}" ${CHAT_CONFIG_FILE}
	@cd ${BASHO_BENCH_DIR}; ./basho_bench --results-dir="${test_dir}" ${CHAT_CONFIG_FILE}
	Rscript --vanilla ${BASHO_BENCH_DIR}/priv/summary.r -i ${test_dir}/current
	@rm ${BASHO_BENCH_DIR}/${CHAT_CONFIG_FILE}