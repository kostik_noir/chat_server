-module(rest_users).

% cowboy_rest callbacks
-export([
  init/3,
  allowed_methods/2,
  content_types_accepted/2,
  content_types_provided/2
]).

% accept resource
-export([
  process_post_request/2
]).

% provide resource
-export([
  process_get_request/2
]).

-include("../include/content_type.hrl").
-include("../include/http_method.hrl").

%------------------------------------------------
% cowboy_rest callbacks
%------------------------------------------------
init(_Transport, _Req, []) ->
  {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
  Methods = [
    ?METHOD_GET,
    ?METHOD_POST
  ],
  {Methods, Req, State}.

content_types_accepted(Req, State) ->
  {Method, Req2} = cowboy_req:method(Req),
  Types =
    case Method of
      ?METHOD_POST ->
        [{?CONTENT_TYPE_FORM_URLENCODED, process_post_request}]
    end,
  {Types, Req2, State}.

content_types_provided(Req, State) ->
  Types = [
    {?CONTENT_TYPE_JSON, process_get_request}
  ],
  {Types, Req, State}.

%------------------------------------------------
% accept resource
%------------------------------------------------
process_post_request(Req, State) ->
  Response =
    case guru_auth:register() of
      {ok, UserId} ->
        S = "{
          \"result\": \"success\",
          \"data\": {
            \"user_id\": \"~s\"
          }
        }",
        lists:flatten(io_lib:format(S, [UserId]));
      {error, Reason} ->
        S = "{
          \"error\": \"~s\"
        }",
        lists:flatten(io_lib:format(S, [Reason]))
    end,
  Req2 = cowboy_req:set_resp_body(Response, Req),
  {true, Req2, State}.

%------------------------------------------------
% provide resource
%------------------------------------------------
process_get_request(Req, State) ->
  Response = {[
    {result, comming_soon}
  ]},
  {jiffy:encode(Response), Req, State}.

%------------------------------------------------
% private
%------------------------------------------------