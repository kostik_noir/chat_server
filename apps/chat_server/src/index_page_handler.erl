-module(index_page_handler).

% cowboy_rest callbacks
-export([
  init/3,
  allowed_methods/2,
  content_types_provided/2
]).

% provide resource
-export([
  process_get_request/2
]).

-include("../include/content_type.hrl").
-include("../include/http_method.hrl").

%------------------------------------------------
% cowboy_rest callbacks
%------------------------------------------------
init(_Transport, _Req, []) ->
  {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
  Methods = [
    ?METHOD_GET
  ],
  {Methods, Req, State}.

content_types_provided(Req, State) ->
  Types = [
    {?CONTENT_TYPE_TEXT_HTML, process_get_request}
  ],
  {Types, Req, State}.

%------------------------------------------------
% provide resource
%------------------------------------------------
process_get_request(Req, State) ->
  {<<"Hi">>, Req, State}.

%------------------------------------------------
% private
%------------------------------------------------