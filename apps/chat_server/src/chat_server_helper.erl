-module(chat_server_helper).

% API
-export([
  get_request_parameters/2
]).

-include("../include/parameters.hrl").

% -----------------------------------------------
% API
% -----------------------------------------------
get_request_parameters(Req, ParamsSpecs) ->
  get_request_parameters(Req, ParamsSpecs, []).

% -----------------------------------------------
% private
% -----------------------------------------------
get_request_parameters(Req, [Spec | ParamsSpecs], Acc) ->
  Result =
    case Spec of
      {?PARAMETER_TYPE_BINDING, ParamName} ->
        get_binding_parameter(Req, ParamName);
      {?PARAMETER_TYPE_BODY, ParamName} ->
        get_body_parameter(Req, ParamName)
    end,
  case Result of
    {ok, Value} -> get_request_parameters(Req, ParamsSpecs, [Value | Acc]);
    {error, undefined} -> get_request_parameters(Req, ParamsSpecs, [undefined | Acc]);
    {error, timeout} -> {error, timeout};
    {error, closed} -> {error, timeout}
  end;
get_request_parameters(_Req, [], Acc) ->
  lists:reverse(Acc).

get_binding_parameter(Req, ParameterName) ->
  case cowboy_req:binding(ParameterName, Req) of
    {error, timeout} -> {error, timeout};
    {error, closed} -> {error, closed};
    {undefined, _Req2} -> {error, undefined};
    {Value, _Req2} -> {ok, Value}
  end.

get_body_parameter(Req, ParameterName) ->
  case cowboy_req:has_body(Req) of
    false ->
      {error, undefined};
    true ->
      case cowboy_req:body_qs(Req) of
        {error, timeout} -> {error, timeout};
        {error, closed} -> {error, closed};
        {ok, FormData, _Req1} ->
          case proplists:lookup(ParameterName, FormData) of
            none -> {error, undefined};
            {_Key, Value} -> {ok, Value}
          end;
        Err -> Err
      end
  end.