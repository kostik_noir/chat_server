-module(rest_members).

% cowboy_rest callbacks
-export([
  init/3,
  allowed_methods/2,
  content_types_accepted/2,
  content_types_provided/2
]).

% accept resource
-export([
  process_post_request/2
]).

% provide resource
-export([
  process_get_request/2
]).

-include("../include/content_type.hrl").
-include("../include/http_method.hrl").
-include("../include/parameters.hrl").

%------------------------------------------------
% cowboy_rest callbacks
%------------------------------------------------
init(_Transport, _Req, []) ->
  {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
  Methods = [
    ?METHOD_GET,
    ?METHOD_POST
  ],
  {Methods, Req, State}.

content_types_accepted(Req, State) ->
  {Method, Req2} = cowboy_req:method(Req),
  Types =
    case Method of
      ?METHOD_POST ->
        [{?CONTENT_TYPE_FORM_URLENCODED, process_post_request}]
    end,
  {Types, Req2, State}.

content_types_provided(Req, State) ->
  Types = [
    {?CONTENT_TYPE_JSON, process_get_request}
  ],
  {Types, Req, State}.

%------------------------------------------------
% accept resource
%------------------------------------------------
process_post_request(Req, State) ->
  Specs = [
    {?PARAMETER_TYPE_BINDING, ?PARAMETER_ROOM_ID},
    {?PARAMETER_TYPE_BODY, ?PARAMETER_USER_ID},
    {?PARAMETER_TYPE_BODY, ?PARAMETER_MEMBER_NAME}
  ],

  Series = [
    % validate parameters
    fun(_Acc) ->
      case chat_server_helper:get_request_parameters(Req, Specs) of
        {error, Reason} -> {error, Reason};
        [undefined, _UserId, _MemberName] ->
          {error, incorrect_room_id};
        [_RoomId, undefined, _MemberName] ->
          {error, incorrect_user_id};
        [_RoomId, _UserId, undefined] ->
          {error, incorrect_member_name};
        [RoomId, UserId, MemberName] ->
          {ok, [RoomId, UserId, MemberName]}
      end
    end,

    % add member
    fun(Acc) ->
      [{ok, [RoomId, UserId, MemberName]} | _Tail] = Acc,
      case chat_room:add_member(RoomId, UserId, MemberName) of
        {error, Reason} ->
          {error, Reason};
        {ok, _Msg} ->
          {result, success}
      end
    end
  ],

  [Result | _Tail] = guru_utils:in_series(Series),
  Req2 = cowboy_req:set_resp_body(jiffy:encode({[Result]}), Req),
  {true, Req2, State}.

%------------------------------------------------
% provide resource
%------------------------------------------------
process_get_request(Req, State) ->
  Response = {[
    {result, comming_soon}
  ]},
  {jiffy:encode(Response), Req, State}.

%------------------------------------------------
% private
%------------------------------------------------