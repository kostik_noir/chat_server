-module(chat_server_app).

-behaviour(application).

% application callbacks
-export([
  start/2,
  stop/1
]).

%-----------------------------------------------
% Application callbacks
%-----------------------------------------------
start(_StartType, _StartArgs) ->
  start_cowboy(),
  chat_server_sup:start_link().

stop(_State) ->
  ok.

%-----------------------------------------------
% private
%-----------------------------------------------
start_cowboy() ->
  Dispatch = cowboy_router:compile([
    {"api.:_", [
      {"/users", rest_users, []},
      {"/rooms", rest_rooms, []},
      {"/rooms/:roomId/members", rest_members, []},
      {"/rooms/:roomId/messages", rest_messages, []},
      {"/ws", ws_handler, []}
    ]},
    {'_', [
      {"/", index_page_handler, []}
    ]}
  ]),
  Port = port(),
  {ok, _} = cowboy:start_http(http, 100, [{port, Port}], [{env, [{dispatch, Dispatch}]}]).

port() ->
  case os:getenv("PORT") of
    false ->
      {ok, Port} = application:get_env(http_port),
      Port;
    Other ->
      list_to_integer(Other)
  end.