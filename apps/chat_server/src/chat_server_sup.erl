-module(chat_server_sup).

-behaviour(supervisor).

% API
-export([
  start_link/0
]).

% supervisor callbacks
-export([
  init/1
]).

-define(WORKER_SHUTDOWN, 1000).

-define(CHILD(Module, Type, Restart, Shutdown),
  {Module, {Module, start_link, []}, Restart, Shutdown, Type, [Module]}).

%------------------------------------------------
% API
%------------------------------------------------
start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%------------------------------------------------
% supervisor callbacks
%------------------------------------------------
init([]) ->
  ChildSpec = [
%     ?CHILD(chat_registry, worker, permanent, ?WORKER_SHUTDOWN),
%     ?CHILD(chat_room_sup, supervisor, permanent, infinity)
  ],
  {ok, {{one_for_one, 5, 10}, ChildSpec}}.

