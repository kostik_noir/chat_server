-module(ws_handler).

-behaviour(cowboy_websocket_handler).

% cowboy_websocket_handler callbacks
-export([
  init/3,
  websocket_init/3,
  websocket_handle/3,
  websocket_info/3,
  websocket_terminate/3
]).

-define(MSG_TYPE_COMMAND, <<"cmd">>).
-define(CMD_LOGIN, <<"login">>).

-include("../include/parameters.hrl").

-record(state, {
  user_id
}).

%------------------------------------------------
% cowboy_websocket_handler callbacks
%------------------------------------------------
init({tcp, http}, _Req, _Opts) ->
  {upgrade, protocol, cowboy_websocket}.

websocket_init(_TransportName, Req, _Opts) ->
  {ok, Req, #state{}}.

websocket_handle(Data, Req, State) ->
  Response =
    case process_ws_message(Data) of
      {ok, Msg} ->
        guru_utils:encode_json({[
          {result, Msg}
        ]});
      {error, Reason} ->
        guru_utils:encode_json({[
          {error, Reason}
        ]})
    end,
  {reply, {text, Response}, Req, State}.

websocket_info({logged_in, UserId}, Req, State) ->
  guru_user:add_listener(UserId),
  {ok, Req, State#state{user_id = UserId}};

websocket_info({chat_room_message, Msg}, Req, State) ->
  Payload = room_message_to_json(Msg),
  {reply, {text, Payload}, Req, State};

websocket_info(_Info, Req, State) ->
  {ok, Req, State}.

websocket_terminate(_Reason, _Req, _State) ->
  ok.

%------------------------------------------------
% private
%------------------------------------------------
process_ws_message({text, Text}) ->
  Series = [
    % decode JSON
    fun(_Acc) ->
      guru_utils:decode_json(Text)
    end,

    % validate message type
    fun([{ok, Msg} | _Tail]) ->
      case Msg of
        {[{?MSG_TYPE_COMMAND, CommandName}, Data]} ->
          process_cmd(CommandName, Data);
        _ ->
          {error, unknown_message_structure}
      end
    end
  ],

  [Result | _Tail] = guru_utils:in_series(Series),
  Result;
process_ws_message(_Other) ->
  {error, unknown_format}.

process_cmd(?CMD_LOGIN, Data) ->
  Series = [
    % validate data
    fun(_Acc) ->
      case Data of
        {?PARAMETER_USER_ID, UserId} ->
          {ok, guru_utils:to_binary(UserId)};
        _ ->
          {error, incorrect_user_id}
      end
    end,

    % check user exists
    fun([{ok, UserId} | _Tail]) ->
      case guru_auth:has_user(UserId) of
        {ok, _} ->
          {ok, UserId};
        _ ->
          {error, incorrect_user_id}
      end
    end,

    fun([{ok, UserId} | _Tail]) ->
      self() ! {logged_in, UserId},
      {ok, success}
    end
  ],
  [Result | _Tail] = guru_utils:in_series(Series),
  Result;
process_cmd(_, _) ->
  {error, unknown_command}.

room_message_to_json(Msg) ->
  {RoomId, SenderId, Content, Timestamp} = Msg,
  Result = {[
    {room_message, {[
      {time, Timestamp},
      {room_id, RoomId},
      {user_id, SenderId},
      {content, Content}
    ]}}
  ]},
  guru_utils:encode_json(Result).