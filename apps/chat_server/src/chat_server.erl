-module(chat_server).

% API
-export([
  start/0
]).

start() ->
  ok = guru_utils:start_app(chat_server),
  ok.