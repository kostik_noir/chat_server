-define(PARAMETER_ROOM_ID, roomId).
-define(PARAMETER_USER_ID, <<"user_id">>).
-define(PARAMETER_MEMBER_NAME, <<"member_name">>).
-define(PARAMETER_ROOM_MESSAGE, <<"message">>).

-define(PARAMETER_TYPE_BINDING, binding).
-define(PARAMETER_TYPE_BODY, body).