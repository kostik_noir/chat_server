-module(chat_db_room_redis).

% API
-export([
  checkin/0,
  checkout/1,
  create/1,
  add_member/4,
  get_members/2,
  add_message/5,
  check_before_add_member/4,
  check_before_add_message/3
]).

-include("../include/pools.hrl").

%------------------------------------------------
% API
%------------------------------------------------
checkin() ->
  poolboy:checkout(?POOL_CHAT).

checkout(Worker) ->
  poolboy:checkin(?POOL_CHAT, Worker).

create(Worker) ->
  RoomId = guru_utils:uuid(),
  RoomId2 = guru_utils:to_list(RoomId),
  Cmd = cmd_create(RoomId2),
  case query(Worker, Cmd) of
    {ok, _V} -> {ok, RoomId2};
    Err -> Err
  end.

add_member(Worker, RoomId, UserId, MemberName) ->
  RoomId2 = guru_utils:to_list(RoomId),
  UserId2 = guru_utils:to_list(UserId),
  MemberName2 = guru_utils:to_list(MemberName),
  Cmds = [
    cmd_save_member_id(RoomId2, UserId2),
    cmd_save_member_name(RoomId2, MemberName2)
  ],
  case multi(Worker, Cmds) of
    {ok, _V} -> {ok, true};
    Err -> Err
  end.

get_members(Worker, RoomId) ->
  RoomId2 = guru_utils:to_list(RoomId),
  Cmd = cmd_get_members(RoomId2),
  case query(Worker, Cmd) of
    {ok, Members} -> {ok, Members};
    Err -> Err
  end.

add_message(Worker, RoomId, UserId, Msg, Time) ->
  RoomId2 = guru_utils:to_list(RoomId),
  UserId2 = guru_utils:to_list(UserId),
  Msg2 = guru_utils:to_list(Msg),
  Cmd = cmd_add_message(RoomId2, UserId2, Msg2, Time),
  case query(Worker, Cmd) of
    {ok, _} -> {ok, true};
    Err -> Err
  end.

check_before_add_member(Worker, RoomId, UserId, MemberName) ->
  RoomId2 = guru_utils:to_list(RoomId),
  UserId2 = guru_utils:to_list(UserId),
  MemberName2 = guru_utils:to_list(MemberName),
  Cmds = [
    cmd_has_room(RoomId2),
    cmd_has_member(RoomId2, UserId2),
    cmd_has_member_name(RoomId2, MemberName2)
  ],
  ParseResults =
    fun(R) ->
      case R of
        [<<"0">>, _, _] -> {error, incorrect_room_id};
        [_, <<"1">>, _] -> {error, already_member};
        [_, _, <<"1">>] -> {error, member_name_taken};
        _ -> {ok, true}
      end
    end,
  case multi(Worker, Cmds) of
    {ok, Results} -> ParseResults(Results);
    Err -> Err
  end.

check_before_add_message(Worker, RoomId, UserId) ->
  RoomId2 = guru_utils:to_list(RoomId),
  UserId2 = guru_utils:to_list(UserId),
  Cmds = [
    cmd_has_room(RoomId2),
    cmd_has_member(RoomId2, UserId2)
  ],
  ParseResults =
    fun(R) ->
      case R of
        [<<"0">>, _] -> {error, incorrect_room_id};
        [_, <<"0">>] -> {error, not_a_member};
        _ -> {ok, true}
      end
    end,
  case multi(Worker, Cmds) of
    {ok, Results} -> ParseResults(Results);
    Err -> Err
  end.

%------------------------------------------------
% private
%------------------------------------------------

%============================
% build keys
%============================
get_room_key(RoomId) ->
  lists:flatten(io_lib:format("guru_chat_room:~s", [RoomId])).

get_room_members_key(RoomId) ->
  lists:flatten(io_lib:format("guru_chat_room:~s:members", [RoomId])).

get_room_member_names_key(RoomId) ->
  lists:flatten(io_lib:format("guru_chat_room:~s:member_names", [RoomId])).

get_room_messages_key(RoomId) ->
  lists:flatten(io_lib:format("guru_chat_room:~s:messages", [RoomId])).

%============================
% redis commands
%============================
cmd_create(RoomId) ->
  Key = get_room_key(RoomId),
  ["HMSET", Key, "name", "true", "created", guru_utils:now()].

cmd_has_room(RoomId) ->
  Key = get_room_key(guru_utils:uuid_to_list(RoomId)),
  ["EXISTS", Key].

cmd_save_member_id(RoomId, UserId) ->
  Key = get_room_members_key(RoomId),
  ["SADD", Key, UserId].

cmd_get_members(RoomId) ->
  Key = get_room_members_key(RoomId),
  ["SMEMBERS", Key].

cmd_has_member(RoomId, UserId) ->
  Key = get_room_members_key(RoomId),
  ["SISMEMBER", Key, UserId].

cmd_save_member_name(RoomId, MemberName) ->
  Key = get_room_member_names_key(RoomId),
  ["SADD", Key, MemberName].

cmd_has_member_name(RoomId, MemberName) ->
  Key = get_room_member_names_key(RoomId),
  ["SISMEMBER", Key, MemberName].

cmd_add_message(RoomId, UserId, Msg, Time) ->
  Key = get_room_messages_key(RoomId),
  ["ZADD", Key, Time, guru_utils:to_binary({UserId, Msg})].

%============================
% call DB
%============================
query(Worker, Cmd) ->
  gen_server:call(Worker, {query, Cmd}).

multi(Worker, Cmds) ->
  gen_server:call(Worker, {multi, Cmds}).
%%
%% db_query(Cmd) ->
%%   Worker = poolboy:checkout(?POOL_CHAT),
%%   Result = gen_server:call(Worker, {query, Cmd}),
%%   poolboy:checkin(?POOL_CHAT, Worker),
%%   Result.
%%
%% db_multi(Cmds) ->
%%   Worker = poolboy:checkout(?POOL_CHAT),
%%   Result = gen_server:call(Worker, {multi, Cmds}),
%%   poolboy:checkin(?POOL_CHAT, Worker),
%%   Result.