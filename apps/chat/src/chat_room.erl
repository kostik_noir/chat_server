-module(chat_room).

% API
-export([
  create/0,
  add_member/3,
  add_message/4
]).

create() ->
  Worker = chat_db_room_redis:checkin(),
  Result = chat_db_room_redis:create(Worker),
  chat_db_room_redis:checkout(Worker),
  Result.

add_member(RoomId, UserId, MemberName) ->
  Worker = chat_db_room_redis:checkin(),
  Series = [
    fun(_Acc) ->
      case chat_db_room_redis:check_before_add_member(Worker, RoomId, UserId, MemberName) of
        {ok, true} -> {ok, true};
        Err -> Err
      end
    end,

    fun(_Acc) ->
      case chat_db_room_redis:add_member(Worker, RoomId, UserId, MemberName) of
        {ok, _} -> {ok, true};
        Err -> Err
      end
    end
  ],
  [Result | _T] = guru_utils:in_series(Series),
  chat_db_room_redis:checkout(Worker),
  Result.

add_message(RoomId, UserId, Msg, Time) ->
  Worker = chat_db_room_redis:checkin(),
  Series = [
    fun(_Acc) ->
      case chat_db_room_redis:check_before_add_message(Worker, RoomId, UserId) of
        {ok, true} -> {ok, true};
        Err -> Err
      end
    end,

    % save
    fun(_Acc) ->
      case chat_db_room_redis:add_message(Worker, RoomId, UserId, Msg, Time) of
        {ok, _} -> {ok, true};
        Err -> Err
      end
    end,

    % notify
    fun(_Acc) ->
      case chat_db_room_redis:get_members(Worker, RoomId) of
        {ok, Users} ->
          Message = {RoomId, UserId, Msg, Time},
          notify_user(Users, {chat_room_message, Message}),
          {ok, true};
        Err -> Err
      end
    end
  ],
  [Result | _T] = guru_utils:in_series(Series),
  chat_db_room_redis:checkout(Worker),
  Result.

notify_user([UserId | Tail], Msg) ->
  guru_user:notify_user(UserId, Msg),
  notify_user(Tail, Msg);
notify_user([], _Msg) ->
  ok.