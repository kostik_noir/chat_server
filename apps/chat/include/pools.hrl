-define(POOL_CHAT, pool_chat).

-define(POOLS, [
  {?POOL_CHAT, [
    {name, {local, ?POOL_CHAT}},
    {size, 100},
    {max_overflow, 100},
    {worker_module, guru_redis_worker}
  ], []}
]).
