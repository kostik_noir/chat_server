-define(POOL_GURU_AUTH, pool_guru_auth).

-define(POOLS, [
  {?POOL_GURU_AUTH, [
    {name, {local, ?POOL_GURU_AUTH}},
    {size, 30},
    {max_overflow, 30},
    {worker_module, guru_redis_worker}
  ], []}
]).
