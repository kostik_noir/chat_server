-module(guru_auth).

% API
-export([
  start/0,
  login/2,
  register/0,
  has_user/1
]).

start() ->
  ok = guru_utils:start_app(chat_auth),
  ok.

login(_UserName, _Password) ->
  ok.

register() -> guru_auth_db_user_redis:register().

has_user(UserId) -> guru_auth_db_user_redis:has_user(UserId).