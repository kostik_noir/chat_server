-module(guru_auth_db_user_redis).

% API
-export([
  login/2,
  register/0,
  has_user/1
]).

-include("../include/pools.hrl").

% -----------------------------------------------
% API
% -----------------------------------------------
login(_UserName, _Password) ->
  ok.

register() ->
  UserId = guru_utils:uuid(),
  Key = get_user_key(UserId),
  Cmd = ["HSET", Key, "name", "true"],
  case call_db(Cmd) of
    {ok, _V} -> {ok, UserId};
    Err -> Err
  end.

has_user(UserId) ->
  Key = get_user_key(guru_utils:uuid_to_list(UserId)),
  Cmd = ["EXISTS", Key],
  case call_db(Cmd) of
    {ok, <<"1">>} -> {ok, true};
    {ok, <<"0">>} -> {ok, false};
    Err -> Err
  end.

% -----------------------------------------------
% private
% -----------------------------------------------
call_db(Cmd) ->
  Worker = poolboy:checkout(?POOL_GURU_AUTH),
  Result = gen_server:call(Worker, {query, Cmd}),
  poolboy:checkin(?POOL_GURU_AUTH, Worker),
  Result.

get_user_key(UserId) ->
  lists:flatten(io_lib:format("guru_user:~s", [UserId])).