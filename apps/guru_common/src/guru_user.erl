-module(guru_user).

% API
-export([
  add_listener/1,
  notify_user/2
]).

add_listener(UserId) ->
  gproc:reg({p, l, user_key(UserId)}),
  ok.

notify_user(UserId, Msg) ->
  Key = user_key(UserId),
  gproc:send({p, l, Key}, {self(), Key, Msg}),
  ok.

user_key(UserId) ->
  {user, UserId}.