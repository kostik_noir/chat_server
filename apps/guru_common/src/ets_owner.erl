-module(ets_owner).

-behaviour(gen_server).

% gen_server callbacks
-export([
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3
]).

%% API
-export([
  start_link/1,
  stop/1,
  create_tables/3,
  await_ets_transfer/1
]).

-record(state, {}).

%------------------------------------------------
% API
%------------------------------------------------
start_link(Name) ->
  gen_server:start_link({local, Name}, ?MODULE, [], []).

stop(Name) ->
  gen_server:cast(Name, stop).

create_tables(Name, Specs, AcceptorPid) ->
  gen_server:cast(Name, {create_tables, Specs, AcceptorPid}).

await_ets_transfer(Count) when Count > 0 ->
  receive
    {'ETS-TRANSFER', _, _, _} -> await_ets_transfer(Count - 1);
    _ -> await_ets_transfer(Count)
  end;
await_ets_transfer(_Count) ->
  ok.

%------------------------------------------------
% gen_server callbacks
%------------------------------------------------
init([]) ->
  {ok, #state{}}.

handle_call(_Msg, _From, State) ->
  {reply, ok, State}.

handle_cast({create_tables, Specs, AcceptorPid}, State) ->
  [create_table(TableName, Options, AcceptorPid) || {TableName, Options} <- Specs],
  {noreply, State};

handle_cast(stop, State) ->
  {stop, normal, State};

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(_Msg, State) ->
  {noreply, State}.

terminate(_Reason, State) ->
  {ok, State}.

code_change(_OldVersion, State, _Extra) ->
  {ok, State}.

%------------------------------------------------
% private
%------------------------------------------------
create_table(TableName, Options, AcceptorPid) ->
  Self = self(),
  case ets:info(TableName, owner) of
    Self -> ok;
    undefined ->
      ets:new(TableName, [{heir, Self, []} | Options])
  end,
  ets:give_away(TableName, AcceptorPid, []).