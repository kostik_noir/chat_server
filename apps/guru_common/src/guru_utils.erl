-module(guru_utils).

% API
-export([
  now/0,
  in_series/1,
  start_app/1,
  poolboy_specs/1,
  to_binary/1,
  to_list/1,
  uuid/0,
  uuid_to_list/1,
  uuid_to_binary/1,
  decode_json/1,
  encode_json/1
]).

now() ->
  {MegaSecs, Secs, MicroSecs} = os:timestamp(),
  MegaSecs * 1000000 * 1000000 + Secs * 1000000 + MicroSecs.

in_series(List) ->
  in_series(List, []).

in_series([Func | List], Acc) ->
  Result = Func(Acc),
  case Result of
    {error, _Reason} ->
      [Result | Acc];
    _ ->
      in_series(List, [Result | Acc])
  end;
in_series([], Acc) ->
  Acc.

start_app(App) ->
  start(App, application:start(App)).

start(_App, ok) ->
  ok;
start(_App, {error, {already_started, _App}}) ->
  ok;
start(App, {error, {not_started, Dep}}) ->
  ok = start_app(Dep),
  start_app(App);
start(App, {error, Reason}) ->
  erlang:error({app_start_failed, App, Reason}).

poolboy_specs(Pools) ->
  lists:map(fun({PoolName, PoolArgs, WorkerArgs}) ->
    poolboy:child_spec(PoolName, PoolArgs, WorkerArgs)
  end, Pools).

uuid() -> euuid:format(euuid:v4()).

uuid_to_list(U) when is_list(U) -> U;
uuid_to_list(U) when is_binary(U) -> uuid:to_string(U).

uuid_to_binary(U) when is_binary(U) -> U;
uuid_to_binary(U) when is_list(U) -> uuid:to_binary(U).

to_binary(Value) when is_binary(Value) -> Value;
to_binary(Value) when is_list(Value) -> list_to_binary(Value);
to_binary(Value) when is_integer(Value) -> to_binary(integer_to_list(Value));
to_binary(Value) when is_tuple(Value) -> to_binary(tuple_to_list(Value)).

to_list(Value) when is_list(Value) -> Value;
to_list(Value) when is_binary(Value) -> binary_to_list(Value);
to_list(Value) when is_integer(Value) -> integer_to_list(Value).

decode_json(Value) ->
  try
    Msg = jiffy:decode(Value),
    {ok, Msg}
  catch
    _:_ -> {error, incorrect_json}
  end.

encode_json(Value) ->
  try
    Json = jiffy:encode(Value),
    {ok, Json}
  catch
    _:_ -> {error, incorrect_jiffy_input}
  end.