% TODO expose more rich API

-module(guru_redis_worker).

-behaviour(gen_server).

% gen_server callbacks
-export([
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3
]).

%% API
-export([
  start_link/1
]).

-define(SERVER, ?MODULE).

-record(state, {
  connection
}).

%------------------------------------------------
% API
%------------------------------------------------
start_link(Args) ->
  gen_server:start_link(?MODULE, [Args], []).

%------------------------------------------------
% gen_server callbacks
%------------------------------------------------
init(_Args) ->
  process_flag(trap_exit, true),
  Conn = connect(),
  {ok, #state{connection = Conn}}.

handle_call({query, Cmd}, _From, #state{connection = Conn} = State) ->
  {reply, query(Conn, Cmd), State};

handle_call({multi, Cmds}, _From, #state{connection = Conn} = State) ->
  {reply, transaction(Conn, Cmds), State};

handle_call(_Msg, _From, State) ->
  {reply, ok, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(_Msg, State) ->
  {noreply, State}.

terminate(_Reason, State) ->
  disconnect(State#state.connection),
  {ok, State}.

code_change(_OldVersion, State, _Extra) ->
  {ok, State}.

%------------------------------------------------
% private
%------------------------------------------------
connect() ->
  {ok, Conn} = eredis:start_link(),
  Conn.

disconnect(Connection) ->
  eredis:stop(Connection).

query(Conn, Cmd) ->
  case eredis:q(Conn, Cmd) of
    {ok, Value} -> {ok, Value};
    {error, Reason} -> {error, Reason}
  end.

transaction(Conn, Cmds) ->
  transaction_start(Conn, Cmds).

transaction_start(Conn, Cmds) ->
  case eredis:q(Conn, ["MULTI"]) of
    {ok, _} ->
      transaction_step(Conn, Cmds),
      transaction_end(Conn);
    Err -> Err
  end.

transaction_end(Conn) ->
  case eredis:q(Conn, ["EXEC"]) of
    {ok, Results} -> {ok, Results};
    Err -> Err
  end.

transaction_step(Conn, [Cmd | Tail]) ->
  case eredis:q(Conn, Cmd) of
    {ok, _Result} -> transaction_step(Conn, Tail);
    Err -> Err
  end;
transaction_step(_Conn, []) ->
  ok.