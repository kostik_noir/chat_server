-module(hackney_test_driver).

% API
-export([
  new/1,
  run/4
]).

-define(HEADER_CONTENT_TYPE, {<<"Content-Type">>, <<"application/x-www-form-urlencoded">>}).

-define(RAW_RESPONSE_INCORRECT_ROOM_ID, <<"{\"error\":\"incorrect_room_id\"}">>).
-define(RAW_RESPONSE_INCORRECT_USER_ID, <<"{\"error\":\"incorrect_user_id\"}">>).
-define(RAW_RESPONSE_INCORRECT_MESSAGE_CONTENT, <<"{\"error\":\"incorrect_message_content\"}">>).
-define(RAW_RESPONSE_SUCCESS, <<"{\"result\":\"success\"}">>).

-define(RESPONSE_INCORRECT_ROOM_ID, {[{<<"error">>, <<"incorrect_room_id">>}]}).
-define(RESPONSE_INCORRECT_USER_ID, {[{<<"error">>, <<"incorrect_user_id">>}]}).
-define(RESPONSE_INCORRECT_MESSAGE_CONTENT, {[{<<"error">>, <<"incorrect_message_content">>}]}).
-define(RESPONSE_SUCCESS, {[{<<"result">>, <<"success">>}]}).

-record(state, {
  user_id,
  room_id,
  messages_url,
  message_payload
}).

new(_Id) ->
  ok = guru_utils:start_app(hackney),
  case prepare() of
    {ok, RoomId, UserId} ->
      {ok, #state{
        user_id = UserId,
        room_id = RoomId,
        messages_url = "http://api.localhost:8080/rooms/" ++ guru_utils:to_list(RoomId) ++ "/messages",
        message_payload = guru_utils:to_binary("user_id=" ++ guru_utils:to_list(UserId) ++ "&message=msg")
      }};
    {error, Reason} -> {stop, Reason}
  end.

prepare() ->
  Series = [
    fun(_Acc) ->
      do_register_user()
    end,
    fun(_Acc) ->
      do_create_room()
    end,
    fun([{ok, RoomId}, {ok, UserId}]) ->
      do_add_member(RoomId, UserId)
    end
  ],
  [Result | Tail] = guru_utils:in_series(Series),
  case Result of
    {error, Reason} -> {error, Reason};
    {ok, _} ->
      [{ok, RoomId}, {ok, UserId}] = Tail,
      {ok, RoomId, UserId}
  end.

do_register_user() ->
  Url = "http://api.localhost:8080/users",
  Headers = [?HEADER_CONTENT_TYPE],
  Method = post,
  Payload = [],
  Options = [],

  case send_request(Method, Url, Headers, Payload, Options) of
    {ok, Body} ->
      case decode_json(Body) of
        {error, Reason} -> {error, {register_user_error, Reason}};
        {ok, Response} ->
          case Response of
            {[{<<"result">>, <<"success">>}, {<<"data">>, {[{<<"user_id">>, UserId}]}}]} -> {ok, UserId};
            {[{<<"error">>, Reason}]} -> {error, {register_user_error, Reason}}
          end
      end;
    {error, Reason} -> {error, {register_user_error, Reason}}
  end.

do_create_room() ->
  Url = "http://api.localhost:8080/rooms",
  Headers = [?HEADER_CONTENT_TYPE],
  Method = post,
  Payload = [],
  Options = [],

  case send_request(Method, Url, Headers, Payload, Options) of
    {ok, Body} ->
      case decode_json(Body) of
        {error, Reason} -> {error, {create_room_error, Reason}};
        {ok, Response} ->
          case Response of
            {[{<<"result">>, <<"success">>}, {<<"data">>, {[{<<"room_id">>, RoomId}]}}]} -> {ok, RoomId};
            {[{<<"error">>, Reason}]} -> {error, {create_room_error, Reason}}
          end
      end;
    {error, Reason} -> {error, {create_room_error, Reason}}
  end.

do_add_member(RoomId, UserId) ->
  Url = "http://api.localhost:8080/rooms/" ++ guru_utils:to_list(RoomId) ++ "/members",
  Headers = [?HEADER_CONTENT_TYPE],
  Method = post,
  Payload = guru_utils:to_binary(
    "user_id=" ++ guru_utils:to_list(UserId) ++ "&member_name=user_" ++ guru_utils:to_list(UserId)),
  Options = [],

  case send_request(Method, Url, Headers, Payload, Options) of
    {ok, Body} ->
      case decode_json(Body) of
        {error, Reason} -> {error, {add_member_error, Reason}};
        {ok, Response} ->
          case Response of
            ?RESPONSE_SUCCESS -> {ok, ok};
            {[{<<"error">>, Reason}]} -> {error, {add_member_error, Reason}}
          end
      end;
    {error, Reason} -> {error, {add_member_error, Reason}}
  end.

run(add_message, _KeyGen, _ValueGen, State) ->
  Url = State#state.messages_url,
  Method = post,
  Headers = [?HEADER_CONTENT_TYPE],
  Payload = State#state.message_payload,
  Options = [],
  case send_request(Method, Url, Headers, Payload, Options) of
    {ok, Body} ->
      case decode_json(Body) of
        {error, Reason} -> {error, Reason};
        {ok, Response} ->
          case Response of
            ?RESPONSE_SUCCESS -> {ok, State};
            {[{<<"error">>, Reason}]} -> {error, Reason, State}
          end
      end;
    % skip eaddrinuse error
    {error, eaddrinuse} ->
      {silent, State};
    {error, Reason} ->
      io:format("~p~n", [Reason]),
      {error, Reason, State}
  end.

send_request(Method, Url, Headers, Payload, Options) ->
  case hackney:request(Method, Url, Headers, Payload, Options) of
    {ok, _StatusCode, _RespHeaders, ClientRef} ->
      Response = hackney:body(ClientRef),
      hackney:close(ClientRef),
      Response;
    Err -> Err
  end.

decode_json(Value) ->
  case Value of
    ?RAW_RESPONSE_INCORRECT_ROOM_ID -> {ok, ?RESPONSE_INCORRECT_ROOM_ID};
    ?RAW_RESPONSE_INCORRECT_USER_ID -> {ok, ?RESPONSE_INCORRECT_USER_ID};
    ?RAW_RESPONSE_INCORRECT_MESSAGE_CONTENT -> {ok, ?RESPONSE_INCORRECT_MESSAGE_CONTENT};
    ?RAW_RESPONSE_SUCCESS -> {ok, ?RESPONSE_SUCCESS};
    Other ->
      try
        Msg = jiffy:decode(Other),
        {ok, Msg}
      catch
        _:_ -> {error, incorrect_json}
      end
  end.